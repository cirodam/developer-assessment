import React from 'react';
import { Link } from 'react-router-dom';

import styles from './Navbar.module.css';

/* The navbar component for the application*/ 

const Navbar = () => {
    return (
        <nav className={styles.navbar}>
            <h1 className={styles.navHeader}>Front-End Developer Assessment</h1>
            <ul className={styles.navLinks}>
                <li className={styles.navLink}><Link to="/"><span className={styles.hideMobile}>Task</span>1</Link></li>
                <li className={styles.navLink}><Link to="/urgentcare"><span className={styles.hideMobile}>Task</span>2</Link></li>
                <li className={styles.navLink}><Link to="/suggestions"><span className={styles.hideMobile}>Task</span>3</Link></li>
            </ul>
        </nav>
    )
}

export default Navbar;
