
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

import CareTypes from './pages/CareTypes';
import UrgentCares from './pages/UrgentCares';
import Navbar from './components/Navbar';

import './App.css';
import Suggestion from './pages/Suggestion';

//The application component with the BrowserRouter

function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Routes>
          <Route exact path="/" element={<CareTypes />}/>
          <Route exact path="/urgentcare" element={<UrgentCares />}/>
          <Route exact path="/suggestions" element={<Suggestion />}/>
        </Routes>
      </Router>
    </>
  );
}

export default App;
