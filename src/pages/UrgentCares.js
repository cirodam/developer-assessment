import React, { useEffect, useState } from 'react';
import axios from 'axios';
import UrgentCareChecker from "urgent-care-checker";
import 'urgent-care-checker/dist/index.css'

import styles from './UrgentCares.module.css';

const UrgentCares = () => {

    const [urgents, setUrgents] = useState([]);

    //Fetch the Urgent Care data from the API
    useEffect(() => {
        
        const getPost = async () => {
            let post = await axios.get("https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/urgents");
            console.log(post.data);
            setUrgents(post.data);
        }

        getPost();

    }, []);

    //Return the page
    return (
        <div className={styles.urgentCares}>
            <h2 className={styles.devNote}>I created an NPM package that lists the wait times of several urgent cares. This information is fetched automatically via my AWS API.</h2>
            <UrgentCareChecker urgents={urgents}/>
            <div className={styles.devLinks}>
                <a href="https://www.npmjs.com/package/urgent-care-checker" className={styles.devLink} aria-label="NPM Package" target="_blank" rel="noopener noreferrer" ><i className={`fab fa-npm ${styles.devIcon}`}></i></a>
                <a href="https://bitbucket.org/cirodam/urgent-care-checker/src/master/" className={styles.devLink} aria-label="Bitbucket Repository" target="_blank" rel="noopener noreferrer" ><i className={`fab fa-bitbucket ${styles.devIcon}`}></i></a>
            </div>
        </div>
    )
}

export default UrgentCares
