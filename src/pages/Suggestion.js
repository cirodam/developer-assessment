import React, { useEffect, useState } from 'react';
import axios from 'axios';
import parse from 'react-html-parser';

import styles from './Suggestion.module.css';

const Suggestion = () => {

    const [post, setPost] = useState(null);

    //Fetch the post from the API
    useEffect(() => {
        
        const getPost = async () => {
            let p = await axios.get(`https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/suggestions`);
            setPost(p.data.post);
        }

        getPost();

    }, []);

    return (

        <div className={styles.suggestions}>

            <h2 className={styles.postTitle}>My Suggestions</h2>
            <a href="https://www.nghs.com/locations/gainesville/" className={styles.subHeader} target="_blank" rel="noopener noreferrer" >Original Page <i className={`fas fa-chevron-right ${styles.arrow}`}></i></a>
            <div className={styles.postContent}>
                { post && parse(post.content.rendered) }
            </div>
        </div>
    )
}

export default Suggestion
