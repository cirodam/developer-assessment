
import styles from './CareTypes.module.css';

function CareTypes() {

  return (

    <div className={styles.careTypes}>
      <h2 className={styles.mainHeader}>How We Provide Care</h2>

      {/* Section 1 */}
      <section className={styles.careSection}>
        <div className={styles.careContainer}>
          <h3 className={styles.careHeader}>Primary Care</h3>
          <p className={styles.careDesc}>This should be your first stop if you’re needing routine, non-emergency care. Your primary care physician knows
            you and your health best. Our providers offer patient-centered care in family medicine and internal medicine to help
            treat common illnesses, as well as more complex health conditions.</p>
          <div className={styles.flex}>
            <div className={styles.careCol}>
              <h4 className={styles.colHeader}>How they provide care:</h4>
              <ul className={styles.colList}>
                <li>Routine check ups</li>
                <li>Preventative care and sick visits</li>
                <li>Treatment for non-urgent, long-term health issues</li>
                <li>Prescribes and manages medications</li>
                <li>Specialist referrals</li>
              </ul>
            </div>
            <div className={styles.careCol}>
              <h4 className={styles.colHeader}>Advantages of this type of care:</h4>
              <ul className={styles.colList}>
                <li>Helps you focus on health and well-being</li>
                <li>Coordinates with your specialists</li>
                <li>Walks alongside you to ensure long-term health</li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      {/* Section 2 */}
      <section className={styles.careSection}>
        <div className={styles.careContainer}>
          <h3 className={styles.careHeader}>Urgent Care</h3>
          <p className={styles.careDesc}>If your primary care doctor is not available, NGPG Urgent Care Clinics provide care for non-life-threatening
              medical conditions. You do not need an appointment, but you can save your spot online and visit after hours and on
              weekends.
          </p>
          <div className={styles.flex}>
            <div className={styles.careCol}>
              <h4 className={styles.colHeader}>Common types of conditions seen at Urgent Care:</h4>
              <ul className={styles.colList}>
                <li>Upper respiratory infection (such as bronchitis)</li>
                <li>Suspected broken bones</li>
                <li>Cuts requiring stitches</li>
                <li>Infections, flu and strep throat</li>
              </ul>
            </div>
            <div className={styles.careCol}>
              <h4 className={styles.colHeader}>Advantages of this type of care:</h4>
              <ul className={styles.colList}>
                <li>Open 8 AM to 8 PM, 7 days a week</li>
                <li>6 locations across Northeast Georgia</li>
                <li>Reserve Your Spot Online check-in</li>
                <li>Access to board-certified providers</li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      {/* Section 3 */}
      <section className={styles.careSection}>
        <div className={styles.careContainer}>
          <h3 className={styles.careHeader}>Emergency Care</h3>
          <p className={styles.careDesc}>Emergency room care should be reserved for very serious or life-threatening problems. At our ERs, you’ll receive
              high-quality emergency care from a specialized team of board-certified emergency medicine physicians, expert
              nurses, paramedics and technicians, 24 hours a day, seven days a week.
          </p>
          <div className={styles.flex}>
            <div className={styles.careCol}>
              <h4 className={styles.colHeader}>Types of conditions seen at the ER:</h4>
              <ul className={styles.colList}>
                <li>Chest pain or pressure</li>
                <li>Severe abdominal pain</li>
                <li>Severe burns</li>
                <li>Difficulty breathing or shortness of breath</li>
                <li>Numbness in the face, arm or leg</li>
                <li>Seizures</li>
                <li>Any condition you believe is life-threatening</li>
              </ul>
            </div>
            <div className={styles.careCol}>
              <h4 className={styles.colHeader}>Advantages of this type of care:</h4>
              <ul className={styles.colList}>
                <li>Open 24 hours a day, 7 days a week</li>
                <li>6 locations across Northeast Georgia</li>
                <li>Staffed by board-certified emergency medicine physicians</li>
                <li>4 locations across Northeast Georgia</li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      {/* Section 4 */}
      <section className={styles.careSection}>
        <div className={styles.careContainer}>
          <h3 className={styles.careHeader}>Video Visits</h3>
          <p className={styles.careDesc}>During a virtual visit, you will connect directly through a phone or computer with a NGPG provider who can
            diagnose and treat your health need. This means you can avoid traveling to a clinic location. Video visits are
            available after hours and on weekends for various specialties, including primary care, internal medicine, pediatrics
            and more.
          </p>
          <div className={styles.flex}>
            <div className={styles.careCol}>
              <h4 className={styles.colHeader}>Advantages of this type of care:</h4>
              <ul className={styles.colList}>
                <li>Connect with your care team through your phone or computer</li>
                <li>Available for a variety of specialties, including primary care, internal medicine, pediatrics, and more</li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      {/* Section 5 */}
      <section className={styles.careSection}>
        <div className={styles.careContainer}>
          <h3 className={styles.careHeader}>After-Hours Care (Telemed)</h3>
          <p className={styles.careDesc}>With Telemed, you can get quick answers to your healthcare questions at no charge! If you are an established patient
            (including pediatrics) and have a non-emergent issue after hours or on the weekend, you can connect with a NGPG
            on-call provider by calling your provider's regular business line and holding until the Telemed service answers.
          </p>
          <div className={styles.flex}>
            <div className={styles.careCol}>
              <h4 className={styles.colHeader}>About:</h4>
              <ul className={styles.colList}>
                <li>Must be a current NGPG patient</li>
                <li>Open to all ages (including pediatrics)</li>
              </ul>
            </div>
            <div className={styles.careCol}>
              <h4 className={styles.colHeader}>How it works:</h4>
              <ul className={styles.colList}>
                <li>Call your doctor's office after business hours</li>
                <li>Hold on the line for the Telemed service to answer</li>
                <li>Provide the Telemed representative with your name, birth date, and reason for calling</li>
                <li>The on-call provider will return your call within an hour</li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      {/* Section 6 */}
      <section className={styles.careSection}>
        <div className={styles.careContainer}>
          <h3 className={styles.careHeader}>E-Visits</h3>
          <h2 className={styles.careSubheader}>Online Symptom Check and Treatment</h2>
          <p className={styles.careDesc}>E-Visits are a convenient, fast and effective option for a few basic healthcare issues – just by filling out a form on
              your computer or smartphone. They’re low-cost and available 24/7. There is a standard $40 cash fee for each e-visit.
          </p>
          <div className={styles.flex}>
            <div className={styles.careCol}>
              <h4 className={styles.colHeader}>Types of conditions seen:</h4>
              <ul className={styles.colList}>
                <li>Flu screening and treatment</li>
                <li>COVID-19 screening</li>
                <li>Sinus problems and treatment</li>
                <li>Urinary problems</li>
                <li>Vaginal discharge</li>
              </ul>
            </div>
            <div className={styles.careCol}>
              <h4 className={styles.colHeader}>Advantages of this type of care:</h4>
              <ul className={styles.colList}>
                <li>Fast</li>
                <li>Affordable</li>
                <li>Secure</li>
                <li>Available to everyone 18 and older</li>
                <li>Insurance will not be filed</li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      <div className={styles.devLinks}>
          <a href="https://bitbucket.org/cirodam/developer-assessment/src/master/" className={styles.devLink} aria-label="Bitbucket Repository" target="_blank" rel="noopener noreferrer" ><i className={`fab fa-bitbucket ${styles.devIcon}`}></i></a>
      </div>

    </div>
  );
}

export default CareTypes;
